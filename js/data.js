var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa khoa tỉnh Đồng Nai",
   "address": "2 Đường Đồng Khởi, Tam Hoà, Biên Hòa, Đồng Nai",
   "Longtitude": 10.952684,
   "Latitude": 106.867852
 },
 {
   "STT": 2,
   "Name": "Bệnh viên Đa khoa Thống Nhất",
   "address": "Phường Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.965936,
   "Latitude": 106.885724
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Đa khoa thành phố Biên Hòa",
   "address": "Phường Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.95291,
   "Latitude": 106.852712
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Phổi Đồng Nai",
   "address": "Xã Phước Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.875446,
   "Latitude": 106.919007
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Đa khoa khu vực Long Thành",
   "address": "Thị Trấn Long Thành, Huyện Long Thành, Đồng Nai",
   "Longtitude": 10.794172,
   "Latitude": 106.948775
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa khu vực Long Khánh",
   "address": "911 21 Tháng 4, Suối Tre, Long Khánh, Đồng Nai",
   "Longtitude": 10.94136,
   "Latitude": 107.215274
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Đa khoa khu vực Định Quán",
   "address": "Thị Trấn Định Quán, Huyện Định Quán, Đồng Nai",
   "Longtitude": 11.198692,
   "Latitude": 107.363176
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa khoa huyện Trảng Bom",
   "address": "Thị Trấn Trảng Bom, Huyện Trảng Bom -Tỉnh Đồng Nai",
   "Longtitude": 10.948244,
   "Latitude": 107.005899
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Đa khoa huyện Xuân Lộc",
   "address": "Xã Suối Cát, Huyện Xuân Lộc, Tỉnh Đồng Nai",
   "Longtitude": 10.908886,
   "Latitude": 107.368872
 },
 {
   "STT": 10,
   "Name": "Bệnh viện Đa khoa huyện Tân Phú",
   "address": "Thị Trấn Tân Phú, Huyện Tân Phú, Tỉnh Đồng Nai",
   "Longtitude": 11.273672,
   "Latitude": 107.440677
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Đa khoa huyện Vĩnh Cửu",
   "address": "768, Thị Trấn Vĩnh An, Huyện Vĩnh Cửu, Tỉnh Đồng Nai",
   "Longtitude": 11.095157,
   "Latitude": 107.024279
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Vĩnh Cửu -Cơ Sở 2",
   "address": "Xã Thạnh Phú Huyện Vĩnh Cửu Đồng Nai",
   "Longtitude": 11.01307,
   "Latitude": 106.848289
 },
 {
   "STT": 13,
   "Name": "Bệnh viện Đa khoa huyện Nhơn Trạch",
   "address": "Xã Phú Hội, Huyện Nhơn Trạch, Tỉnh Đồng Nai",
   "Longtitude": 10.72682,
   "Latitude": 106.884366
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Nhi Đồng",
   "address": "Phường Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.956182,
   "Latitude": 106.874342
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Đa khoa Dầu Giây",
   "address": "Xã Bàu Hàm 2, Huyện Thống Nhất, Tỉnh Đồng Nai",
   "Longtitude": 10.951526,
   "Latitude": 107.139178
 },
 {
   "STT": 16,
   "Name": "Bệnh viện Cao su Đồng Nai",
   "address": "Xã Suối Tre, Thị Xã Long Khánh, Đồng Nai",
   "Longtitude": 10.945832,
   "Latitude": 107.210296
 },
 {
   "STT": 17,
   "Name": "Bệnh viện Đa khoa huyện Cẩm Mỹ",
   "address": "Xã Long Giao, Huyện Cẩm Mỹ, Tỉnh Đồng Nai",
   "Longtitude": 10.814484,
   "Latitude": 107.218778
 },
 {
   "STT": 18,
   "Name": "Bệnh viện Y Dược cổ truyền",
   "address": "Phường Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.974893,
   "Latitude": 106.848681
 },
 {
   "STT": 19,
   "Name": "Bệnh viện Da liễu tỉnh",
   "address": "Phường Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.999593,
   "Latitude": 106.852988
 },
 {
   "STT": 20,
   "Name": "Bệnh viện Tâm Thần Trung Ương 2",
   "address": "Phường Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.965654,
   "Latitude": 106.846071
 },
 {
   "STT": 21,
   "Name": "Bệnh viện Đa khoa Tâm Hồng Phước",
   "address": "Phường Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.970285,
   "Latitude": 106.866559
 },
 {
   "STT": 22,
   "Name": "Công ty cổ phần Bệnh viện Quốc tế Đồng Nai",
   "address": "Phường Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.958068,
   "Latitude": 106.846896
 },
 {
   "STT": 23,
   "Name": "Công ty cổ phần thương mại Quốc tế Sỹ Mỹ",
   "address": "Phường Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.968208,
   "Latitude": 106.899896
 },
 {
   "STT": 24,
   "Name": "Công ty cổ phần Bệnh viện Quốc tế chấn thương chỉnh hình Sài Gòn - Đồng Nai",
   "address": "Phường Thống Nhất, Biên Hòa, Đồng Nai",
   "Longtitude": 10.947964,
   "Latitude": 106.828759
 }
];